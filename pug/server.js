const express = require('express');
const Contenedor = require('./classes/Contenedor.js');

//Defino variables
let contenedor = new Contenedor('productos.json');
const app = express();
const puerto = 8080;
// incorporo el router
const router_api = express.Router();
const router = express.Router();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//defino ruta base
app.use('/', router_api);
app.use('/', router);
// indico donde estan los archivos estaticos
app.use(express.static('public'));
// seteo el motor de plantilla
app.set('view engine', 'pug');
app.set('views', './views');

// pongo a escuchar el servidor en el puerto indicado
const server = app.listen(puerto, () => {
   //console.log(`servidor escuchando en http://localhost:${puerto}`);
   console.log("Iniciando servidor")   
   //console.log(obj_items)
   console.log( "servidor inicializado en puerto:" + server.address().port)
});

// en caso de error, avisar
server.on('error', error => {
    console.log('error en el servidor:', error);
});

/********************/
/*       WEB        */
/********************/

router.get('/productos', (req, res) => {   
    res.render('vista.pug', {  productos: contenedor.getAllSync(), hasProductos: (1> 0) });
});

router.get('/agregar', (req, res) => {   
    res.render('grabar.pug', null);
});


/********************/
/*       API        */
/********************/

//TEST
router_api.get('/test', (req, res) => {
    res.send('get recibido!');
});

router_api.post('/test', (req, res) => {
    res.send('post recibido!')
});

//LISTAR
router_api.get('/listar', (req , res)=> {
    //listo todos los productos
    let result = contenedor.getAllSync();
    if (result) {
        console.log(result)
        res.send(result);
    } else {
        res.status(500).json({ error: 'no se pueden obtener los productos' });
    }
} );

//LISTAR UNO
router_api.get('/listar/:id', (req , res)=> {
 //LISTO SOLO 1 Elemento    
        //LISTO SOLO 1 Elemento    
        let result = contenedor.getByIdSync(req.params.id)
        if (result) {
            res.json(result);
        } else {
            res.status(500).json({ error: 'producto no encontrado' });
        }
} );

// CARGA PRODUCTOS
router_api.post('/productos', (req , res)=> {
    if (Array.isArray(req.body)) {
        let added_products = [];
        let json_array = req.body.map(JSON.stringify);
        json_array.forEach(it => {
            let producto = JSON.parse(it);
            let response = contenedor.saveSync(producto)
            added_products.push(response);

        });
        if (added_products.length > 0) {
            //res.json(added_products)
            res.render('grabar.pug', null);
        }
        else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    } else {
        let result = contenedor.saveSync(req.body)
        if (result) {
            //res.json(result)
            res.render('grabar.pug', null);
        } else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    }
} )

// ACTUALIZA PRODUCTOS
router_api.put('/:id', (req , res)=> {
    let result = contenedor.updateByIdSync(req.params.id, req.body);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' actualizado' })
    } else {
        res.status(500).json({ error: 'producto no encontrado' });
    }
} )

// ELIMINA PRODUCTOS
router_api.delete('/:id', (req , res)=> {
    let result = contenedor.deleteByIdSync(req.params.id);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' eliminado' })
    } else {
        res.status(500).json({ resultado: 'id:' + req.params.id + ' No enocontrado' })
    }
} )